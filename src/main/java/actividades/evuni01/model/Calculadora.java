/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividades.evuni01.model;

/**
 *
 * @author Cony
 */
public class Calculadora {
    private double tasaInteres;
    private Integer anios;
    private double capital;

    public Calculadora() {
    }
    
    public double calcularInteres(){
      double resultado = this.capital*(this.tasaInteres/100)*anios; 
      return resultado;  
    } 
    
    

    public double getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    public Integer getAnios() {
        return anios;
    }

    public void setAnios(Integer anios) {
        this.anios = anios;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }
    
    
}
