<%-- 
    Document   : resultado
    Created on : 15-09-2021, 18:02:51
    Author     : Cony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String resultado = (String)request.getAttribute("resultado");
    String capital = (String)request.getAttribute("capital");
    String tasa = (String)request.getAttribute("tasaInteres");
    String anios = (String)request.getAttribute("anios");
    
                %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>
            
            Un capital de $<%=capital%> a una tasa anual de <%=tasa%>% en <%=anios%> años genera un interés simple de: $<%=resultado%>.
                      
        </h1>
    </body>
</html>
