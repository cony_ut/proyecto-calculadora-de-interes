<%-- 
    Document   : index
    Created on : 15-09-2021, 09:49:50
    Author     : Cony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Interés Simple</title>
    </head>
    <body>
        <h1>Calculadora Interés Simple</h1>
        <form action="CalculadoraController" method="POST">
            <div class="form-group">
            <label for="capital"> Ingrese el capital total: </label>
            <input type="capital" name="capital" class="form-control" id="capital">
            </div>
            <div class="form-group">
            <label for="tasaInteres"> Ingrese la tasa de interés anual (%): </label>
            <input type="tasaInteres" name="tasaInteres" class="form-control" id="tasaInteres">
            </div>
            <div class="form-group">
            <label for="anios"> Ingrese el número de años: </label>
            <input type="anios" name="anios" class="form-control" id="anios">
            </div>
            <button type="submit" class="btn btn-default"> Submit </button>
        </form>    
    </body>
</html>
